/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cs2321;

import net.datastructures.NodePositionList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mark
 */
public class Player2Test {

	public Player2Test() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class Player2.
	 */
	@Test
	public void testGetName() {
		System.out.println("getName");
		Player2 instance = new Player2();
		String expResult = "TODO: Needs a name";
		String result = instance.getName();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getStrategy method, of class Player2.
	 */
	@Test
	public void testGetStrategy() {
		System.out.println("getStrategy");
		Player2 instance = new Player2();
		String expResult = "BLACK MAGIC explained in the writeup";
		String result = instance.getStrategy();
		assertEquals(expResult, result);
	}

	/**
	 * Test of setRound method, of class Player2.
	 */
	@Test
	public void testSetRound() {
		System.out.println("setRound");
		int r = 42;
		Player2 instance = new Player2();
		instance.setRound(r);
	}

	/**
	 * Test of setMap method, of class Player2.
	 */
	@Test (expected = Error.class)
	public void testSetMap() {
		System.out.println("setMap");
		Player2 instance = new Player2();
		instance.setMap(null);
	}

	/**
	 * Test of setTarget method, of class Player2.
	 */
	@Test
	public void testSetTarget() {
		System.out.println("setTarget");
		MyVertex v = new MyVertex(42);
		Player2 instance = new Player2();
		instance.setTarget(v);
		assertEquals(instance.getTarget(),v);
	}

	/**
	 * Test of currentPosition method, of class Player2.
	 */
	@Test
	public void testCurrentPosition() {
		System.out.println("currentPosition");
		Player2 instance = setupData();
		System.out.println(instance.getPos());
		assertTrue(instance.getPos().equals((Integer)33));
	}

	/**
	 * Test of availableMoves method, of class Player2.
	 */
	@Test
	public void testAvailableMoves() {
		System.out.println("availableMoves");
		Player2 instance = setupData();
		//FUCK FUCKING NODEPOSITIONLIST IT DOSN"T HAVE A FUCKING COMPARE TO. WHY MUST WE USE THIS SHIT.
		assertEquals(instance.getAvailableMoves().first().element(), 5);
		assertEquals(instance.getAvailableMoves().last().element(), 42);
	}

	/**
	 * Test of recommendedMove method, of class Player2.
	 */
	@Test
	public void testRecommendedMove() {
		System.out.println("recommendedMove");
		Player2 instance = setupData();
		instance.recommendedMove(42);
		assertEquals( ((Integer)instance.getRecommendedMove()).intValue(), 42);//It treats the output as an object, so we get to have fun happyCasting times.
	}

	/**
	 * Test of recommendMove method, of class Player2.
	 */
	@Test (expected = Error.class)
	public void testRecommendMove() {
		System.out.println("recommendMove");
		Player2 instance = new Player2();
		Object result = instance.recommendMove();
	}

	/**
	 * Test of makeMove method, of class Player2.
	 * Also tests the
	 */
	@Test
	public void testMakeMove() {
		System.out.println("makeMove");
		Player2 instance = setupData();

		instance.willYouComply();
		Integer result = (Integer)instance.makeMove();
		assertFalse(42 == result.intValue());//Not recom
	}

	/**
	 * Test of willYouComply method, of class Player2.
	 */
	@Test
	public void testWillYouComply() {
		System.out.println("willYouComply");
		Player2 instance = setupData();
		boolean result = instance.willYouComply();
	}

	/**
	 * setupData creates a player object with some move data so we can test things like willYouComply or MakeMove
	 * @return
	 */
	private Player2 setupData(){//TODO
		Player2<Integer,Integer> instance = new Player2();

		instance.currentPosition(33);
		NodePositionList<Integer> moves = new NodePositionList<>();
		moves.addLast(5);
		moves.addLast(2);
		moves.addLast(42);
		instance.availableMoves(moves);
		instance.recommendedMove(42);

		return instance;
	}
}
