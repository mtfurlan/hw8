
package cs2321;

import net.datastructures.NodePositionList;
import net.datastructures.Vertex;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * CLASSNAME: Player1Test.java
 * @author Kathryn Weinand
 * @author Mark Furland
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 8
 * DESCRIPTION: Tests Player1's methods
 * ANALYSIS: The space complexity is O(v+e), where v is the number of
 *           vertices in the graph and e is the number of edges because
 *           it has to access memory allocated for the game board.
 * NOTES:
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/12/13 Kathryn Weinand: Started
 */
@SpaceComplexity("O(v+e)")
public class Player1Test {

	/**
	 * Test of getName method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testGetName() {
		System.out.println("getName");
		Player1<String,Integer> instance = new Player1<String, Integer>();
		String result = instance.getName();
		assertTrue(result.length() > 3);
		if(result.compareTo("Ever-Victorious, Iron-Willed Commander of Computers")!=0)
			fail("Result string is not the expected name.");
	}

	/**
	 * Test of getStrategy method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testGetStrategy() {
		System.out.println("getStrategy");
		Player1<String,Integer> instance = new Player1<String, Integer>();
		String result = instance.getStrategy();
		assertTrue(result.length() > 3);
		if(result.compareTo("Recommend the next move on the shortest path from Player2's current position to Player1's target")!=0)
			fail("Result string is not the expected name.");
	}

	/**
	 * Test of setRound method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testSetRound() {
		System.out.println("setRound");
		int r = 0;
		Player1<String,Integer> instance = new Player1<String, Integer>();
		instance.setRound(r);
		if(instance.getRound() != 0)
			fail("Round not set correctly");
	}

	/**
	 * Test of setMap method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(v)")
	public void testSetMap() {
		System.out.println("setMap");
		Player1<String, Integer> instance = new Player1<String,Integer>();
		MainGraph<String, Integer> map = new MainGraph<String, Integer>();
		Vertex<String> Home = map.insertVertex("Home");
		Vertex<String> Houghton =map.insertVertex("Houghton");
		map.insertEdge(Home, Houghton, 6);
		instance.setMap(map);
		if(!instance.getMap().equals(map))
			fail("Map not set correctly");
	}

	/**
	 * Test of setTarget method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testSetTarget() {
		System.out.println("setTarget");
		String v = "Home";
		Player1<String, Integer> instance = new Player1<String, Integer>();
		instance.setTarget(v);
		if(!instance.getTarget().equals(v))
			fail("Target not set correctly");
	}

	/**
	 * Test of currentPosition method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testCurrentPosition() {
		System.out.println("currentPosition");
		String v = "Home";
		Player1<String, Integer> instance = new Player1<String, Integer>();
		instance.currentPosition(v);
		if(!instance.getCurrentPosition().equals(v))
			fail("Current position not set correctly");
	}

	/**
	 * Test of availableMoves method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testAvailableMoves() {
		System.out.println("availableMoves");
		Player1<String, Integer> instance = new Player1<String, Integer>();
		NodePositionList<Integer> v = new NodePositionList<Integer>();
		v.addLast(6);
		v.addLast(2);
		v.addLast(13);
		v.addLast(5);
		instance.availableMoves(v);
		if(!instance.getAvailableMoves().equals(v))
			fail("Available moves not set correctly");
	}

	/**
	 * Test of recommendedMove method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testRecommendedMove() {
		System.out.println("recommendedMove");
		boolean err = false;
		Player1<String, Integer> instance = new Player1<String, Integer>();
		try{instance.recommendedMove(8);} catch(Error e){err = true;}
		if(!err)
			fail("recommendedMove should have thrown an error as it is only a Player2 method");
	}

	/**
	 * Test of recommendMove method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(v+e)")
	public void testRecommendMove() {
		System.out.println("recommendMove");
		MainGraph<Character,Integer> g = new MainGraph<Character,Integer>();
		Vertex<Character> A = g.insertVertex('A');
		Vertex<Character> B = g.insertVertex('B');
		Vertex<Character> C = g.insertVertex('C');
		Vertex<Character> D = g.insertVertex('D');
		Vertex<Character> E = g.insertVertex('E');
		Vertex<Character> F = g.insertVertex('F');
		Vertex<Character> H = g.insertVertex('H');
		Vertex<Character> I = g.insertVertex('I');
		Vertex<Character> J = g.insertVertex('J');
		Vertex<Character> L = g.insertVertex('L');
		g.insertEdge(A, F, 2);
		g.insertEdge(A, B, 1);
		g.insertEdge(A, C, 1);
		g.insertEdge(L, F, 1);
		g.insertEdge(L, D, 1);
		g.insertEdge(D, B, 3);
		g.insertEdge(B, E, 1);
		g.insertEdge(B, I, 1);
		g.insertEdge(B, H, 1);
		g.insertEdge(H, I, 1);
		g.insertEdge(H, J, 1);
		g.insertEdge(H, C, 1);
		Player1<Character, Integer> instance = new Player1<Character,Integer>();
		instance.setMap(g);
		instance.currentPosition(F.element());
		instance.setTarget(J.element());
		if(instance.recommendMove().compareTo(2) != 0)
			fail("Shortest path of UNWEIGHTED GRAPH failed");
		instance.currentPosition(B.element());
		instance.setTarget(L.element());
		if(instance.recommendMove().compareTo(3) != 0)
			fail("Shortest path of UNWEIGHTED GRAPH failed");
	}

	/**
	 * Test of makeMove method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testMakeMove() {
		System.out.println("makeMove");
		boolean err = false;
		Player1<String, Integer> instance = new Player1<String, Integer>();
		try{instance.makeMove();} catch(Error e){err = true;}
		if(!err)
			fail("makeMove should have thrown an error as it is only a Player2 method");
	}

	/**
	 * Test of willYouComply method, of class Player1.
	 */
	@Test
	@TimeComplexity("O(1)")
	public void testWillYouComply() {
		System.out.println("willYouComply");
		boolean err = false;
		Player1<String, Integer> instance = new Player1<String, Integer>();
		try{instance.willYouComply();} catch(Error e){err = true;}
		if(!err)
			fail("willYouComply should have thrown an error as it is only a Player2 method");
	}
}
