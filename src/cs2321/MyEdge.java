package cs2321;

import net.datastructures.Edge;
import net.datastructures.Position;
import net.datastructures.Vertex;

/**
 * CLASSNAME: MyEdge.java
 * @author Roberto Tamassia
 * @author Kathryn Weinand
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 7
 * @param <V> : generic vertex type
 * @param <E> : generic edge type
 * DESCRIPTION: Builds and maintains a vertex.
 * ANALYSIS: The space complexity is O(n+m) where n is the
 *           number of incidence edges on one end vertex and
 *           m the number of incidence edges on the other.
 * NOTES: This is the same class as the jar file with an extra
 *        parameter for the vertices. Because I did not write this
 *        class, but repurposed it from given material that wouldn't
 *        ordinarily be commented as throughly, comments are sparse.
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/11/25 Kathryn Weinand: Copied from jar file (GraphStuff)
 */
@SpaceComplexity("O(n+m)")
public class MyEdge<V,E> extends MyPosition<E> implements Edge<E> {

	/** The end vertices of the edge. */
	protected MyVertex<V,E>[] endVertices;
	/** The positions of the entries for the edge in the incidence
	 * containers of the end vertices. */
	protected Position<Edge<E>>[] Inc;
	/** The position of the edge in the edge container of the
	 * graph. */
	protected Position<Edge<E>> loc;

	/** Constructs an edge with the given endpoints and elements. */
	public MyEdge (Vertex<V> v, Vertex<V> w, E o) {
		elem = o;
		endVertices = (MyVertex<V,E>[]) new MyVertex[2];
		endVertices[0] = (MyVertex<V,E>)v;
		endVertices[1] = (MyVertex<V,E>)w;
		Inc = (Position<Edge<E>>[]) new Position[2];
	}
	/** Returns the end vertices of the edge. There are always two
	 * elements in the returned array. */
	public MyVertex<V,E>[] endVertices() {
		return endVertices;
	}
	/** Returns the positions of the edge in the incidence containers
	 * of its end vertices.  The returned array always contains two
	 * elements. */
	public Position<Edge<E>>[] incidences() {
		return Inc;
	}
	/** Sets the positions of the edge in the incidence containers of
	 * its end vertices. */
	public void setIncidences(Position<Edge<E>> pv, Position<Edge<E>> pw) {
		Inc[0] = pv;
		Inc[1] = pw;
	}
	/** Returns the position of the edge in the edge container of the
	 * graph. */
	public Position<Edge<E>> location() {
		return loc;
	}
	/** Sets the position of the edge in the edge container of the
	 * graph. */
	public void setLocation(Position<Edge<E>> p) {
		loc = p;
	}
	/** Returns a string representation of the edge via a tuple of
	 * vertices. */
	public String toString() {
		return element() + "(" + endVertices[0].toString() +
				", " + endVertices[1].toString() + ")";
	}
	
	//TODO comments
	public boolean antiVertex(V v){
		//Make sure that given vertex isn't from here.
		//Also only vertice 1 should be null ever and I dont' know why.
		boolean ret = true;;
		if(endVertices()[0].element() != null){
			if(endVertices()[0].element().equals(v)){
				ret = false;
			}
		}
		if(endVertices()[1].element() != null){
			if(endVertices()[1].element().equals(v)){
				ret = false;
			}
		}
		return ret;
//		return (endVertices()[0] != null && !endVertices()[0].element().equals(v)) || (endVertices()[1] != null && !endVertices()[1].element().equals(v));
	}
	
	public Vertex<V> opposite(V v){
		if(endVertices()[0].element() == v){
			return endVertices()[1];
		}else if(endVertices()[1].element() == v){
			return endVertices()[0];
		}
		throw new Error("You asked for the opposite node to a node not on the edge");
	}
}
