package cs2321;

import net.datastructures.EmptyQueueException;
import net.datastructures.NodePositionList;
import net.datastructures.Queue;

/**
 * CLASSNAME: MyQueue.java
 * @author Kathryn Weinand
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 8
 * @param <T> : generic element type
 * DESCRIPTION: Builds and maintains a queue.
 * ANALYSIS: The space complexity is O(n) where n is the
 *           number of entries.
 * NOTES:
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/12/05 Kathryn Weinand: Created
 */
@SpaceComplexity("O(n)")
public class MyQueue<T> implements Queue<T>{
	NodePositionList<T> queue;

	/**
	 * METHODNAME: MyQueue
	 * @author Kathryn Weinand
	 * @param
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: The constructor creates the NodePositionList
	 *              containing the queue.
	 * ANALYSIS: The time complexity is O(1) - doesn't
	 *           depend on size.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public MyQueue(){
		queue = new NodePositionList<T>();
	} //end of method: MyQueue

	/**
	 * METHODNAME: size
	 * @author Kathryn Weinand
	 * @param
	 * @return the number of entries in the queue
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the number of entries in the queue.
	 * ANALYSIS: The time complexity of size is O(1) - just returns
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public int size() {
		return queue.size();
	} //end of method: size

	/**
	 * METHODNAME: isEmpty
	 * @author Kathryn Weinand
	 * @param
	 * @return true : if queue has no entries; false : otherwise
	 * @throws
	 * @see
	 * DESCRIPTION: Returns true if queue has no entries and
	 *              false : otherwise
	 * ANALYSIS: The time complexity of isEmpty is O(1) - just returns
	 *           a comparison of an global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	} //end of method: isEmpty

	/**
	 * METHODNAME: front()
	 * @author Kathryn Weinand
	 * @param
	 * @return first element of queue
	 * @throws EmptyQueueException if queue is empty
	 * @see
	 * DESCRIPTION: Returns first element of queue.
	 * ANALYSIS: The time complexity of front is O(1) - just returns
	 *           the first element of a NodePositionList
	 * MODIFICATIONS:
	 *   2013/1/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public T front() throws EmptyQueueException {
		if(isEmpty())
			throw new EmptyQueueException("The queue is empty");
		return queue.first().element();
	} //end of method: front

	/**
	 * METHODNAME: enqueue
	 * @author Kathryn Weinand
	 * @param new element to enqueue
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Adds a new element to the queue.
	 * ANALYSIS: The time complexity of enqueue is O(1) - just adds
	 *           an element to the queue.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void enqueue(T element) {
		queue.addLast(element);

	} //end of method: enqueue

	/**
	 * METHODNAME: dequeue
	 * @author Kathryn Weinand
	 * @param
	 * @return an element off the queue (FIFO)
	 * @throws EmptyQueueException if queue is empty
	 * @see
	 * DESCRIPTION: Removes and returns an element off the queue (FIFO).
	 * ANALYSIS: The time complexity of isEmpty is O(1) - just removes
	 *           and returns the first element of a NodePositionList.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public T dequeue() throws EmptyQueueException {
		if(isEmpty())
			throw new EmptyQueueException("The queue is empty");
		T rem = queue.remove(queue.first());
		return rem;
	} //end of method: dequeue

	/**
	 * METHODNAME: print
	 * @author Kathryn Weinand
	 * @param
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Prints a string representation of the queue.
	 * ANALYSIS: The time complexity is O(n) because every element will
	 *           be visited.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(n)")
	public void print(){
		System.out.println(queue.toString());
	} //end of method: print

	/**
	 * METHODNAME: main
	 * @author Kathryn Weinand
	 * @param args
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Testing.
	 * ANALYSIS: The time complexity is O(n) because every elment needs
	 *           to be visited for the print methods.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(n)")
	public static void main(String[] args) {
		MyQueue<Integer> q = new MyQueue<Integer>();
		q.enqueue(8);
		q.enqueue(9);
		q.enqueue(10);
		q.print();
		q.dequeue();
		q.print();

	} //end of method: main

} //end of class: MyQueue
