package cs2321;

import java.io.PrintWriter;

public class PrintGraph {
	public static void showP2Graph(MainGraph graph,int prefix){
		try{
			PrintWriter writer = new PrintWriter("dotfile" + prefix + ".dot", "UTF-8");
			writer.println(graph.toDotFormat());
			writer.close();
			Runtime.getRuntime().exec("graphviz\\dot.exe -Tjpg dotfile" + prefix + ".dot -o out" + prefix + ".jpg");
		}catch(Exception e){
			System.out.println(e);
		}
	}
}
