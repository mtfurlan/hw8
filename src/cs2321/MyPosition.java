package cs2321;

import net.datastructures.DecorablePosition;
import net.datastructures.HashTableMap;

/**
 * CLASSNAME: MyPosition.java
 * @author Roberto Tamassia
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 7
 * @param <V> : generic vertex type
 * @param <E> : generic edge type
 * DESCRIPTION: Builds and maintains a vertex.
 * ANALYSIS: The space complexity is O(1) - only has to allocate for
 *           the element
 * NOTES: This is the same class as the jar file. Because I did not write this
 *        class, but repurposed it from given material that wouldn't
 *        ordinarily be commented as throughly, comments are sparse.
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/11/25 Kathryn Weinand: Copied from jar file (GraphStuff)
 */
@SpaceComplexity("O(n)")
public class MyPosition<T> extends HashTableMap<Object,Object> implements DecorablePosition<T> {

	/** The element stored at this position. */
	protected T elem;
	/** Returns the element stored at this position. */
	public T element() {
		return elem;
	}
	/** Sets the element stored at this position. */
	public void setElement(T o) {
		elem = o;
	}
}
