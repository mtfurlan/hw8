package cs2321;

import net.datastructures.Edge;
import net.datastructures.NodePositionList;
import net.datastructures.Position;
import net.datastructures.PositionList;
import net.datastructures.Vertex;

/**
 * CLASSNAME: MyVertex.java
 * @author Roberto Tamassia
 * @author Kathryn Weinand
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 8
 * @param <V> : generic vertex type
 * @param <E> : generic edge type
 * DESCRIPTION: Builds and maintains a vertex.
 * ANALYSIS: The space complexity is O(n) where n is the
 *           number of incidence edges.
 * NOTES: This is the same class as the jar file with an extra
 *        parameter for the edges as well as some extra global
 *        variables and their getters and setters. Because I did not
 *        write this class, but repurposed it from given material that
 *        wouldn't ordinarily be commented as throughly, comments are sparse.
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/11/25 Kathryn Weinand: Copied from jar file (GraphStuff)
 *   2013/12/05 Kathryn Weinand: Added global variables and their getters and setters
 */
@SpaceComplexity("O(n)")
public class MyVertex<V, E> extends MyPosition<V> implements Vertex<V> {
	/** Incidence container of the vertex. */
	protected PositionList<Edge<E>> incEdges;
	/** Position of the vertex in the vertex container of the graph. */
	protected Position<Vertex<V>> loc;
	protected MyVertex<V,E> prev;
	protected MyEdge<V,E> pEdge;
	protected boolean visit;
	protected int dist;
	protected boolean recommended;
	/** Constructs the vertex with the given element. */
	public MyVertex(V o) {
		elem = o;
		incEdges = new NodePositionList<Edge<E>>();
		dist = 100000;
		visit = false;
		prev = null;
		recommended = false;
	}
	/** Constructs the vertex with the given element. */
	public MyVertex(V o, boolean recommended) {
		elem = o;
		incEdges = new NodePositionList<Edge<E>>();
		dist = 100000;
		visit = false;
		prev = null;
		this.recommended = recommended;
	}

	/**
	 * METHODNAME: getPEdge
	 * @author Kathryn Weinand
	 * @param
	 * @return this vertex's Previous Edge
	 * @throws
	 * @see
	 * DESCRIPTION: Returns this vertex's Previous Edge.
	 * ANALYSIS: The time complexity of getPEdge is O(1) - just returns
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public MyEdge<V,E> getPEdge(){
		return pEdge;
	} //end of method: getPEdge

	/**
	 * METHODNAME: setPEdge
	 * @author Kathryn Weinand
	 * @param the new previous edge
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets this vertex's Previous Edge.
	 * ANALYSIS: The time complexity of setPEdge is O(1) - just changes
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public void setPEdge(MyEdge<V,E> p){
		pEdge = p;
	} //end of method: setPEdge

	/**
	 * METHODNAME: getPrev
	 * @author Kathryn Weinand
	 * @param
	 * @return this vertex's Previous vertex
	 * @throws
	 * @see
	 * DESCRIPTION: Returns this vertex's previous vertex.
	 * ANALYSIS: The time complexity of getPrev is O(1) - just returns
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public MyVertex<V,E> getPrev(){
		return prev;
	} //end of method: getPrev

	/**
	 * METHODNAME: setPrev
	 * @author Kathryn Weinand
	 * @param the new Previous vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets this vertex's previous vertex.
	 * ANALYSIS: The time complexity of setPrev is O(1) - just changes
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public void setPrev(MyVertex<V,E> p){
		prev = p;
	} //end of method: setPrev

	/**
	 * METHODNAME: getVisit
	 * @author Kathryn Weinand
	 * @param
	 * @return true : if this vertex has been visited; false : otherwise
	 * @throws
	 * @see
	 * DESCRIPTION: Returns true if this vertex has been visited and false otherwise.
	 * ANALYSIS: The time complexity of getVisit is O(1) - just returns
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public boolean getVisit(){
		return visit;
	} //end of method: getVisit

	/**
	 * METHODNAME: setVisit
	 * @author Kathryn Weinand
	 * @param true : if this vertex has been visited; false : otherwise
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets true if this vertex has been visited and false otherwise.
	 * ANALYSIS: The time complexity of setVisit is O(1) - just changes
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public void setVisit(boolean b){
		visit = b;
	} //end of method: setVisit

	/**
	 * METHODNAME: getDist
	 * @author Kathryn Weinand
	 * @param
	 * @return the distance of the shortest path to this vertex.
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the distance of the shortest path to this vertex.
	 * ANALYSIS: The time complexity of getDist is O(1) - just returns
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public int getdist(){
		return dist;
	} //end of method: getDist

	/**
	 * METHODNAME: setDist
	 * @author Kathryn Weinand
	 * @param the new distance of the shortest path to this vertex.
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets the distance of the shortest path to this vertex.
	 * ANALYSIS: The time complexity of getDist is O(1) - just changes
	 *           a global variable.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public void setDist(int d){
		dist = d;
	} //end of method: setDist

	/** Return the degree of a given vertex */
	public int degree() {
		return incEdges.size();
	}
	/** Returns the incident edges on this vertex. */
	public Iterable<Edge<E>> incidentEdges() {
		return incEdges;
	}
	/** Inserts an edge into the incidence container of this vertex. */
	public Position<Edge<E>> insertIncidence(Edge<E> e) {
		incEdges.addLast(e);
		return incEdges.last();
	}
	/** Removes an edge from the incidence container of this vertex. */
	public void removeIncidence(Position<Edge<E>> p) {
		incEdges.remove(p);
	}
	/** Returns the position of this vertex in the vertex container of
	 * the graph. */
	public Position<Vertex<V>> location() {
		return loc;
	}
	/** Sets the position of this vertex in the vertex container of
	 * the graph. */
	public void setLocation(Position<Vertex<V>> p) {
		loc = p;
	}
	/** Returns a string representation of the element stored at this
	 * vertex. */
	public String toString() {
		if(elem == null){
			return "null";
		}
		return elem.toString();
	}
	
	/**
	 * isRecommended is just for the HW8 assignment, it's explained in the writeup.
	 * @return recommended
	 */
	public boolean isRecommended(){
		return recommended;
	}
	/**
	 * resetRecommended is just for the HW8 assignment, it's explained in the writeup.
	 */
	public void resetRecommended(){
		recommended = false;
	}
	/**
	 * resetRecommended is just for the HW8 assignment, it's explained in the writeup.
	 */
	public void setRecommended(){
		recommended = true;
	}

}
