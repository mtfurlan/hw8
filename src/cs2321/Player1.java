package cs2321;

import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.NodePositionList;
import net.datastructures.Player;

/**
 * CLASSNAME: Player1.java
 * @author Kathryn Weinand
 * @author Mark Furland
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 8
 * @param <V> : generic vertex type
 * @param <E> : generic edge type
 * DESCRIPTION: Player 1 of the Game class. This player knows the board
 *              and attempts to steer Player 2 in the direction of Player
 *              1's target.
 * ANALYSIS: The space complexity is O(v+e), where v is the number of
 *           vertices in the graph and e is the number of edges because
 *           it has to access memory allocated for the game board.
 * NOTES:
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/12/05 Kathryn Weinand: Started
 */
@SpaceComplexity("O(v+e)")
public class Player1<V,E> implements Player<V,E>,Player.Human {
	private MainGraph<V,E> map;
	private int round;
	private V target;
	private V curr;
	private NodePositionList<E> moves;
	private E recom;
	private int count;

	/**
	 * METHODNAME: getName
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return the name of Player 1
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the name of Player 1.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public String getName() {
		return "Ever-Victorious, Iron-Willed Commander of Computers";
	} //end of method: getName

	/**
	 * METHODNAME: getStrategy
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return the strategy of Player 1
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the strategy of Player 1.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public String getStrategy() {
		return "Recommend the next move on the shortest path from Player2's current position to Player1's target";
	} //end of method: getStrategy

	/**
	 * METHODNAME: setRound
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param the round number
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets the round number.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void setRound(int r) {
		round = r;
	} //end of method: setRound
	
	/**
	 * METHODNAME: getRound
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param the round number
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Gets the round number. For testing purposes.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/13 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public int getRound() {
		return round;
	} //end of method: getRound

	/**
	 * METHODNAME: setMap
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param the game board map
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets the game board map.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void setMap(Graph<V, E> g) {
		map = (MainGraph<V, E>) g;
	} //end of method: setMap
	
	/**
	 * METHODNAME: getMap
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param the game board map
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Gets the game board map. For testing purposes.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/13 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public Graph<V, E> getMap() {
		return map;
	} //end of method: getMap

	/**
	 * METHODNAME: setTarget
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 1's target vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 1's target vertex.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void setTarget(V v) {
		target = v;
	} //end of method: setTarget
	
	/**
	 * METHODNAME: getTarget
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 1's target vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 1's target vertex. For testing purposes.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/13 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public V getTarget() {
		return target;
	} //end of method: getTarget

	/**
	 * METHODNAME: currentPosition
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's current position vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 2's current position vertex.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void currentPosition(V v) {
		curr = v;
	} //end of method: currentPosition
	
	/**
	 * METHODNAME: getCurrentPosition
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's current position vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Gets Player 2's current position vertex.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/13 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public V getCurrentPosition() {
		return curr;
	} //end of method: getCurrentPosition

	/**
	 * METHODNAME: availableMoves
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's available moves
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 2's available moves NodePositionList.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void availableMoves(Iterable<E> e) {
		moves = (NodePositionList<E>) e;
	} //end of method: availableMoves
	
	/**
	 * METHODNAME: getAvailableMoves
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's available moves
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Gets Player 2's available moves NodePositionList.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/13 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public NodePositionList<E> getAvailableMoves() {
		return moves;
	} //end of method: getAvailableMoves

	/**
	 * METHODNAME: recommendedMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's recommended move from Player 1
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: THIS IS ONLY A PLAYER2 METHOD - NOT IMPLEMENTED HERE.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void recommendedMove(E e) {
		throw new Error("Should not be called, is not for player 1");
	} //end of method: recommendedMove

	/**
	 * METHODNAME: recommendMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 1's recommended move for Player 2
	 * @throws
	 * @see
	 * DESCRIPTION: Player 1 calculates the shortest path from Player 2's
	 *              current position to Player 1's target and recommends
	 *              the next move on that path.
	 * ANALYSIS: The time complexity is O(v+e) because every vertex and every edge can
	 *           be visited in the shortest path.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(v+e)")
	@Override
	public E recommendMove() {
		MyVertex<V,E> c = map.vertExist(curr);
		MyVertex<V,E> t = map.vertExist(target);
		return map.shortestPath(c, t);
	} //end of method: recommendMove

	/**
	 * METHODNAME: makeMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 2's next move
	 * @throws
	 * @see
	 * DESCRIPTION: THIS IS ONLY A PLAYER2 METHOD - NOT IMPLEMENTED HERE.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public E makeMove() {
		throw new Error("Should not be called, is not for player 1");
	} //end of method: makeMove

	/**
	 * METHODNAME: willYouComply
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return true if Player 2 takes Player 1's recommended move; false, otherwise
	 * @throws
	 * @see
	 * DESCRIPTION: THIS IS ONLY A PLAYER2 METHOD - NOT IMPLEMENTED HERE.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public boolean willYouComply() {
		throw new Error("Should not be called, is not for player 1");
	} //end of method: willYouComply

} //end of class: Player1
