package cs2321;

import net.datastructures.Edge;
import net.datastructures.NodePositionList;
import net.datastructures.Player;
import net.datastructures.Vertex;

/** *************************************************************************
 * CLASS: Game<V extends String,E extends String>
 *
 * @param <V> For our purposes, a String
 * @param <E> For our purposes, a String
 *
 * This is the game's program loop.
 *
 * DO NOT MODIFY THIS FILE
 * *********************************************************************** */
public class Game<V extends String,E extends String> {
	public static final String START_POSITION = "START_POSITION";
	public static final String TARGET_A = "TARGET_A";
	public static final String TARGET_B = "TARGET_B";

	MainGraph<V,E> board = null;
	private MainGraph<V,E> getBoard( ) {
		return board;
	}
	private void setBoard(MainGraph<V, E> g) {
		board = g;
	}

	Player1<V,E> player1 = null;
	private Player1<V,E> getPlayer1( ) {
		return player1;
	}
	private void setPlayer1(Player1<V, E> p1) {
		player1 = p1;
	}

	Player2<V,E> player2 = null;
	private Player2<V,E> getPlayer2( ) {
		return player2;
	}
	private void setPlayer2(Player2<V, E> p2) {
		player2 = p2;
	}

	V boardPosition = null;
	private V getBoardPosition( ) {
		return boardPosition;
	}
	private void setBoardPosition(V v) {
		boardPosition = v;
	}

	V player1Target = null;
	private V getPlayer1Target( ) {
		return player1Target;
	}
	private void setPlayer1Target(V v) {
		player1Target = v;
	}

	V player2Target = null;
	private V getPlayer2Target( ) {
		return player2Target;
	}
	private void setPlayer2Target(V v) {
		player2Target = v;
	}

	public Game(String[] args) {
		initializeBoard( );
		initializePosition( );
		initializePlayer1( ); // Sees board. Recommends moves.
		initializePlayer2( ); // Controls position. Makes moves.
	}

	private void initializeBoard() {
		MainGraph<V,E> board = new MainGraph<V,E>( );
		setBoard( board );
		Vertex<V> startPosition = board.insertVertex((V) START_POSITION);
		Vertex<V> targetA = board.insertVertex((V) TARGET_A);
		Vertex<V> targetB = board.insertVertex((V) TARGET_B);
		Vertex<V> v1 = board.insertVertex((V) "TALUS");
		Vertex<V> v2 = board.insertVertex((V) "GOLEM");
		Vertex<V> v3 = board.insertVertex((V) "TIK-TOK");
		Vertex<V> v4 = board.insertVertex((V) "ZAT");
		Vertex<V> v6 = board.insertVertex((V) "GNUT");
		Vertex<V> v10 = board.insertVertex((V) "BORS");
		Vertex<V> v12 = board.insertVertex((V) "GORT");
		Vertex<V> v15 = board.insertVertex((V) "TRURL");
		Vertex<V> v16 = board.insertVertex((V) "MARVIN");
		Vertex<V> v21 = board.insertVertex((V) "CHIP");
		Vertex<V> v22 = board.insertVertex((V) "YOD");
		Vertex<V> v23 = board.insertVertex((V) "ELIO");
		Vertex<V> v25 = board.insertVertex((V) "DATA");
		Edge e0 = board.insertEdge(startPosition, v12, (E) "FLEET");
		Edge e1 = board.insertEdge(startPosition, v12, (E) "ABBEY");
		Edge e2 = board.insertEdge(v12, v6, (E) "PORTOBELLO");
		Edge e3 = board.insertEdge(v12, v10, (E) "WHITEHALL");
		Edge e4 = board.insertEdge(v12, v15, (E) "STRAND");
		Edge e5 = board.insertEdge(v12, v16, (E) "THREADNEEDLE");
		Edge e6 = board.insertEdge(v6, v2, (E) "DOWNING");
		Edge e7 = board.insertEdge(v10, v2, (E) "HARLEY");
		Edge e8 = board.insertEdge(v15, v21, (E) "PARK");
		Edge e9 = board.insertEdge(v16, v21, (E) "REGENT");
		Edge e10 = board.insertEdge(v2, v3, (E) "CARNABY");
		Edge e11 = board.insertEdge(v2, v1, (E) "BOND");
		Edge e12 = board.insertEdge(v21, v25, (E) "SAVILE");
		Edge e13 = board.insertEdge(v21, v22, (E) "COMPTON");
		Edge e14 = board.insertEdge(v3, v4, (E) "PALL");
		Edge e15 = board.insertEdge(v1, v4, (E) "PICADILLY");
		Edge e16 = board.insertEdge(v25, v23, (E) "BAKER");
		Edge e17 = board.insertEdge(v22, v23, (E) "CHANCERY");
		Edge e18 = board.insertEdge(v4, targetA, (E) "CHEAPSIDE");
		Edge e19 = board.insertEdge(v23, targetB, (E) "OXFORD");
	}

	private void initializePosition() {
		setBoardPosition((V) START_POSITION);
		if (Math.random() < 0.5 ) {
			setPlayer1Target((V) TARGET_A);
			setPlayer2Target((V) TARGET_B);
		} else {
			setPlayer1Target((V) TARGET_B);
			setPlayer2Target((V) TARGET_A);
		}
	}

	private void initializePlayer1() {
		setPlayer1( new Player1<V,E>( ) );
		getPlayer1( ).setMap( getBoard() );
		getPlayer1( ).setTarget( getPlayer1Target( ) );
	}

	private void initializePlayer2() {
		setPlayer2( new Player2<V,E>( ) );
		getPlayer2( ).setTarget( getPlayer2Target( ) );
	}

	private void gameLoop() {
		int turn = 0;
		V previousPosition = (V) "";

		printRules( );

		if ( getPlayer1( ) instanceof Player.Human ) {
			System.out.println( "\nPLAYER_1 Game Board: " + getBoard( ).edges() );
		}

		System.out.println( "\nBEGIN" );
		System.out.println( "=====" );

		while( (!getBoardPosition( ).equals( TARGET_A )) &&
				(!getBoardPosition( ).equals( TARGET_B )) &&
				(turn++ <= 500) ) {
			Vertex<V> v = findVertex(  getBoardPosition( ) );
			Vertex<V> pv = findVertex(  previousPosition );
			NodePositionList<E> moves = getMoves( v, pv );
			E recommendedMove = null;
			E actualMove = null;
			previousPosition = getBoardPosition( );

			System.out.println( "ROUND " + turn );
			System.out.println( "BOARD POSITION: " + getBoardPosition( ) );
			System.out.println( "AVAILABLE MOVES: " + moves );

			getPlayer1( ).setRound( turn );
			getPlayer1( ).currentPosition( getBoardPosition( ) );
			getPlayer1( ).availableMoves( moves );
			recommendedMove = getPlayer1( ).recommendMove( );
			System.out.println( "PLAYER_1 SUGGESTS MOVE: " + recommendedMove );
			if ( !isLegalMove( moves, recommendedMove ) ) {
				System.out.println( "PLAYER_1 CHEATED!" );
				break;
			}

			getPlayer2( ).setRound( turn );
			getPlayer2( ).currentPosition( getBoardPosition( ) );
			getPlayer2( ).availableMoves( moves );
			getPlayer2( ).recommendedMove( recommendedMove );
			moves = filterMoves( moves, recommendedMove, getPlayer2( ).willYouComply( ) );
			getPlayer2( ).availableMoves( moves );
			actualMove = getPlayer2( ).makeMove( );
			System.out.println( "PLAYER_2 " + (recommendedMove.equals(actualMove)?"COMPLIES: ":"DISOBEYS AND MOVES: ") + actualMove );
			if ( !isLegalMove( moves, actualMove ) ) {
				System.out.println( "PLAYER_2 CHEATED!" );
				break;
			}

			makeMove(v,actualMove);

			System.out.println( "\n" );
		}

		System.out.println( "GAME OVER");
		System.out.println( "FINAL ROUND " + turn );
		System.out.println( "FINAL BOARD POSITION: " + getBoardPosition( ) );
		System.out.println( "PLAYER_1 TARGET: " + getPlayer1Target( ) );
		System.out.println( "PLAYER_2 TARGET: " + getPlayer2Target( ) );
		if ( getBoardPosition( ).equals( getPlayer1Target( ) ) ) {
			System.out.println( "\nPLAYER_1 WINS!!!");
			System.out.println( "\nPLAYER_1's WINNING STRATEGY: " + getPlayer1( ).getStrategy( ) );
			System.out.println( "PLAYER_2's LOSING STRATEGY: " + getPlayer2( ).getStrategy( ) );
		} else if ( getBoardPosition( ).equals( getPlayer2Target( ) ) ) {
			System.out.println( "\nPLAYER_2 WINS!!!");
			System.out.println( "\nPLAYER_2's WINNING STRATEGY: " + getPlayer2( ).getStrategy( ) );
			System.out.println( "PLAYER_1's LOSING STRATEGY: " + getPlayer1( ).getStrategy( ) );
		} else {
			System.out.println( "\nNO WINNER" );
			System.out.println( "\nPLAYER_1's STRATEGY: " + getPlayer1( ).getStrategy( ) );
			System.out.println( "PLAYER_2's STRATEGY: " + getPlayer2( ).getStrategy( ) );
		}

	}

	private NodePositionList<E> filterMoves(NodePositionList<E> moves, E recommendedMove, boolean comply) {
		NodePositionList<E> result = new NodePositionList<E>( );
		for( E cur: moves) {
			if ( comply && cur.equals(recommendedMove) ) {
				result.addLast( cur );
			} else if ( !comply && !cur.equals(recommendedMove) ) {
				result.addLast( cur );
			}
		}
		return result;
	}
	private void makeMove(Vertex<V> v, E m) {
		Edge<E> e = findEdge( m );
		Vertex<V> p = getBoard( ).opposite(v, e);
		setBoardPosition( p.element( ) );
	}
	private boolean isLegalMove(NodePositionList<E> moves, E move) {
		boolean result = false;
		for( E cur:moves ) {
			if ( cur.equals( move ) ) {
				result = true;
				break;
			}
		}
		return result;
	}
	private void printRules() {
		System.out.println("REFLEXIVE-CONTROL");
		System.out.println("=================");
		System.out.println( "RULES:");
		System.out.println( "1. PLAYER_1 can see the board and suggests moves." );
		System.out.println( "2. PLAYER_2 Decides whether to obey Player_1 and makes moves." );
		System.out.println( "3. PLAYER_1 & PLAYER_2 have different target goals." );
		System.out.println( "4. If no goal is reached after 500 rounds, the game will terminate with no winner." );
//		System.out.println( "5. PLAYERS cannot move back to the most recent position." );

		System.out.println( "\nPLAYER_1: " + getPlayer1( ).getName( ) );
		System.out.println( "PLAYER_2: " + getPlayer2( ).getName( ) );
	}

	private Vertex<V> findVertex(V label) {
		Vertex<V> v = null;
		NodePositionList<Vertex<V>> list = (NodePositionList<Vertex<V>>) getBoard( ).vertices( );
		for( Vertex<V> cur:list ) {
			if ( cur.element().equals( label ) ) {
				v = cur;
				break;
			}
		}
		return v;
	}

	private Edge<E> findEdge(E label) {
		Edge<E> e = null;
		NodePositionList<Edge<E>> list = (NodePositionList<Edge<E>>) getBoard( ).edges( );
		for( Edge<E> cur:list ) {
			if ( cur.element().equals( label ) ) {
				e = cur;
				break;
			}
		}
		return e;
	}

	private NodePositionList<E> getMoves( Vertex<V> v, Vertex<V> pv ) {
		NodePositionList<Edge<E>> list = (NodePositionList<Edge<E>>) getBoard( ).incidentEdges( v );
		NodePositionList<E> result = new NodePositionList<E>( );
		for( Edge<E> cur:list ) {
			if ( //(!getBoard( ).opposite(v, cur).equals(pv)) &&
					(!getBoard( ).opposite(v, cur).element( ).equals(START_POSITION))) {
				result.addLast( cur.element( ) );
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Game<String,String> game = new Game<String,String>( args );
		game.gameLoop( );
	}

}
