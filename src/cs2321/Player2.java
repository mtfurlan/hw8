package cs2321;

import net.datastructures.Graph;
import net.datastructures.NodePositionList;
import net.datastructures.Player;
import net.datastructures.Vertex;

/**
 * CLASSNAME: Player2.java
 * @author Kathryn Weinand
 * @author Mark Furland
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 8
 * @param <V> : generic vertex type
 * @param <E> : generic edge type
 * DESCRIPTION: Player 2 of the Game class. This player does not know
 *              board and decides what moves to make, whether or not it
 *              is the recommended move from Player 1. It will make a
 *              board as it goes to help it make decisions.
 * ANALYSIS: The space complexity is O(v+e), where v is the number of
 *           vertices in the graph and e is the number of edges because
 *           it will make a game board as it goes.
 * NOTES:
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/12/05 Kathryn Weinand: Started
 */
@SpaceComplexity("O(v+e)")
public class Player2<V,E> implements Player<V,E> {
	private int round;
	private V target;
	private V curr;
	private NodePositionList<E> moves;
	private E recom;
	private int count;
	private MainGraph<V,E> internalBoard = null;
	private Vertex<V> lastPos = null;
	private E lastChoice = null;

	/**
	 * METHODNAME: Player2
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets count and internal board for strategy.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public Player2(){
		count = 1;
		internalBoard = new MainGraph<V,E>();
	} //end of method: Player2

	/**
	 * METHODNAME: getName
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return the name of Player 2
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the name of Player 2.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public String getName() {
		return "TODO: Needs a name";
	} //end of method: getName

	/**
	 * METHODNAME: getStrategy
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return the strategy of Player 2
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the strategy of Player 2.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public String getStrategy() {
		return "BLACK MAGIC explained in the writeup";
	} //end of method: getStrategy

	/**
	 * METHODNAME: setRound
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param the round number
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets the round number.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void setRound(int r) {
		round = r;
	} //end of method: setRound

	/**
	 * METHODNAME: setMap
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param the game board map
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets the game board map.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void setMap(Graph<V, E> g) {
		throw new Error("Should not be called, is not for player 2");
	} //end of method: setMap

	/**
	 * METHODNAME: setTarget
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's target vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 2's target vertex.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void setTarget(V v) {
		target = v;
	} //end of method setTarget

	/**
	 * METHODNAME: getTarget
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 2's target vertex
	 * @throws
	 * @see
	 * DESCRIPTION: Gets Player 2's target vertex.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public V getTarget() {
		return target;
	} //end of method: getTarget

	/**
	 * METHODNAME: currentPosition
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's current position vertex
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 2's current position vertex and adds a
	 *              new edge to its internal game board.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void currentPosition(V v) {
		if(lastChoice == null){//If this is setup, just add it
			lastPos = internalBoard.insertVertex(v);
		}else if(internalBoard.vertExist(v) == null){//If not in the board add it
			Vertex newLastPos = internalBoard.insertVertex(v);
			//System.out.println(lastChoice);
			internalBoard.removeEdge(lastChoice);
			internalBoard.insertEdge(lastPos,newLastPos,lastChoice);
			lastPos = newLastPos;
		}//If it's already in the board do nothing\
		lastPos = internalBoard.vertExist(v);
		curr = v;
	} //end of method: currentPosition

	/**
	 * METHODNAME: getPos
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 2's current position vertex
	 * @throws
	 * @see
	 * DESCRIPTION: Gets Player 2's current position vertex.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public V getPos(){
		return curr;
	} //end of method: getPos

	/**
	 * METHODNAME: getInternalBoard
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 2's internal game board
	 * @throws
	 * @see
	 * DESCRIPTION: Gets Player 2's internal game board.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public MainGraph<V,E> getInternalBoard(){
		return internalBoard;
	} //end of method: getInternalBoard

	/**
	 * METHODNAME: availableMoves
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's available moves
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Sets Player 2's available moves (NodePositionList).
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * NOTES:
	 *			This really does require visual testing, the junit tests can't do this.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void availableMoves(Iterable<E> e) {
		moves = (NodePositionList<E>) e;
		for(E edgeVal : moves){
			MyEdge<V,E> edge;
			if((edge = internalBoard.edExist(edgeVal)) != null){//If this edge is in the graph in the graph
				//CHeck if it's not attached not the current node. If it isn't, attach it so.
				if(edge.antiVertex(curr)){//If it's NOT attached to curr
					internalBoard.removeEdge(edge);
					MyVertex<V,E> otherVertex = edge.endVertices()[1];//For some reason the second vertex is the one we know.
					internalBoard.insertEdge(otherVertex, lastPos, edge.element());//We have now connected the otherVertex and current.
				}
				//If both edges are set, we do nothing
			}else{//Edge not in graph
				//Add it and an empty node
				MyVertex<V,E> otherVertex = (MyVertex<V,E>)internalBoard.insertVertex(null);;
				internalBoard.insertEdge(otherVertex, lastPos, edgeVal);//Added edge to empty vertex
			}
		}
	} //end of method: availableMoves

	/**
	 * METHODNAME: getAvailableMoves
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 2's available moves
	 * @throws
	 * @see
	 * DESCRIPTION: Gets Player 2's available moves (NodePositionList).
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public NodePositionList<E> getAvailableMoves(){
		return moves;
	} //end of method: getAvailableMoves

	/**
	 * METHODNAME: recommendedMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's recommended move from Player 1
	 * @return
	 * @throws
	 * @sees
	 * DESCRIPTION: Sets Player 1's recommended move for Player 2.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public void recommendedMove(E e) {
		/*
		 * Note about recommended edges, if you move on them, it gets reset. I don't think this is a problem, and am leaving it.
		 * Just a note.
		 */
		MyEdge<V,E> edge = internalBoard.edExist(e);
		MyVertex<V,E> vertex = (MyVertex<V,E>)edge.opposite(curr);
		if(vertex.element() == null){
			vertex.setRecommended();
		}
		recom = e;
	} //end of method: recommendedMove

	/**
	 * METHODNAME: getRecommendedMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param Player 2's recommended move from Player 1
	 * @return
	 * @throws
	 * @sees
	 * DESCRIPTION: Gets Player 1's recommended move for Player 2.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public E getRecommendedMove() {
		return recom;
	} //end of method: getRecommendedMove

	/**
	 * METHODNAME: recommendMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 1's recommended move for Player 2
	 * @throws
	 * @see
	 * DESCRIPTION: THIS IS ONLY A PLAYER1 METHOD - NOT IMPLEMENTED HERE.
	 * ANALYSIS: The time complexity is O(1) - size doesn't matter
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public E recommendMove() {
		throw new Error("Should not be called, is not for player 2");
	} //end of method: recommendMove

	/**
	 * METHODNAME: makeMove
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return Player 2's next move
	 * @throws
	 * @see
	 * DESCRIPTION: Returns Player 2's next move.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public E makeMove() {
		
		return lastChoice;
	} //end of method: makeMove

	/**
	 * METHODNAME: willYouComply
	 * @author Kathryn Weinand
     * @author Mark Furland
	 * @param
	 * @return true if Player 2 takes Player 1's recommended move; false, otherwise
	 * @throws
	 * @see
	 * DESCRIPTION: Returns true if Player 2 takes Player 1's recommended move and false, otherwise.
	 * ANALYSIS: The time complexity  is O(1) - size doesn't
	 *           matter.
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public boolean willYouComply() {
		/*
		 * So the idea here is to go to the first unexplored node that isn't recommended.
		 * We shall do this by getting a list of nodes, taking the first that is set to null, and is not recommended, and returning the first edge on the path to that node.
		 */
		for(Vertex v : internalBoard.vertices()){
			MyVertex<V,E> vert = (MyVertex)v;
			if(vert.element() == null && !vert.isRecommended() && vert.incidentEdges().iterator().hasNext()){
				//I HAVE NO IDEA WHY I NEED TO CHECK IF IT HAS EDGES. Nothing should be able to not have edges.
				lastChoice = internalBoard.shortestPath(internalBoard.vertExist(curr), vert);
				return lastChoice == recom;
			}
		}
		//If we got here that means there are no verticies that are null and not recommended. So we reset them here.
		throw new Error("We chose next move poorly");
	} //end of method: willYouComply

} //end of class: Player 2
