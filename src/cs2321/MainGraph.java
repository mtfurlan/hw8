package cs2321;

import java.util.Iterator;

import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.InvalidPositionException;
import net.datastructures.NodePositionList;
import net.datastructures.Position;
import net.datastructures.Vertex;

/**
 * CLASSNAME: MainGraph.java
 * @author Kathryn Weinand
 * COURSE: CS2321
 * ASSIGNMENT: Programming Assignment 8
 * @param <V> : generic vertex type
 * @param <E> : generic edge type
 * DESCRIPTION: Builds and maintains a graph using NodePositionLists.
 * ANALYSIS: The space complexity is O(v+e), where v is the number of
 *           vertices in the graph and e is the number of edges.
 * NOTES: The caution signs appear when casting from a Vertex<V> or an
 *        Edge<E> to a MyVertex<V,E> or a MyEdge<V,E>, respectively.
 * TODO: Finished!
 * MODIFICATIONS:
 *   2013/11/25 Kathryn Weinand: Started
 *   2013/12/05 Kathryn Weinand: Added shortest path method
 */
@SpaceComplexity("O(v+e)")
public class MainGraph<V,E> implements Graph<V,E> {
	protected NodePositionList<MyVertex<V,E>> vert;
	protected NodePositionList<MyEdge<V,E>> ed;

	/**
	 * METHODNAME: MainGraph
	 * @author Kathryn Weinand
	 * @param
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: The constructor creates the basis of the
	 *              graph from the NodePositionLists.
	 * ANALYSIS: The time complexity is O(1) - doesn't
	 *           depend on size.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public MainGraph(){
		vert = new NodePositionList<MyVertex<V,E>>();
		ed = new NodePositionList<MyEdge<V,E>>();
	} //end of method: MainGraph

	/**
	 * METHODNAME: numVertices
	 * @author Kathryn Weinand
	 * @param
	 * @return the number of vertices in the graph
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the number of vertices in the graph.
	 * ANALYSIS: The time complexity of size is O(1) - just returns
	 *           a global variable of a global variable.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public int numVertices() {
		assert vert.size() >= 0 : "Size can't be negative.";
		return vert.size();
	} //end of method: numVertices

	/**
	 * METHODNAME: numEdges
	 * @author Kathryn Weinand
	 * @param
	 * @return the number of edges in the graph
	 * @throws
	 * @see
	 * DESCRIPTION: Returns the number of edges in the graph.
	 * ANALYSIS: The time complexity of size is O(1) - just returns
	 *           a global variable of a global variable.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public int numEdges() {
		assert ed.size() >= 0 : "Size can't be negative.";
		return ed.size();
	} //end of method: numEdges

	/**
	 * METHODNAME: vertices
	 * @author Kathryn Weinand
	 * @param
	 * @return an Iterable collection of the vertices in the graph. If no matches
	 *         are found the returned Iterable is empty.
	 * @throws
	 * @see
	 * DESCRIPTION: Return an iterable collection of all vertices in the graph.
	 * ANALYSIS: The time complexity is O(v) because every vertex is visited.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added isEmpty checks!
	 */
	@TimeComplexity("O(v)")
	@Override
	public Iterable<Vertex<V>> vertices() {
		NodePositionList<Vertex<V>> itr = new NodePositionList<Vertex<V>>();
		if(!vert.isEmpty()){
			Position<MyVertex<V,E>> curr = vert.first();
			while(curr != vert.last()){
				itr.addLast(curr.element());
				curr = vert.next(curr);
			}
			itr.addLast(vert.last().element());
		}
		assert itr != null : "The vertex iterable should not be null even when empty.";
		return itr;
	} //end of method: vertices

	/**
	 * METHODNAME: edges
	 * @author Kathryn Weinand
	 * @param
	 * @return an Iterable collection of the edges in the graph. If no matches
	 *         are found the returned Iterable is empty.
	 * @throws
	 * @see
	 * DESCRIPTION: Return an iterable collection of all edges in the graph.
	 * ANALYSIS: The time complexity is O(e) because every edge is visited.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added isEmpty checks!
	 */
	@TimeComplexity("O(e)")
	@Override
	public Iterable<Edge<E>> edges() {
		NodePositionList<Edge<E>> itr = new NodePositionList<Edge<E>>();
		if(!ed.isEmpty()){
			Position<MyEdge<V,E>> curr = ed.first();
			while(curr != ed.last()){
				itr.addLast(curr.element());
				curr = ed.next(curr);
			}
			itr.addLast(ed.last().element());
		}
		assert itr != null : "The vertex iterable should not be null even when empty.";
		return itr;
	} //end of method: edges

	/**
	 * METHODNAME: replace
	 * @author Kathryn Weinand
	 * @param the vertex whose element will be changed
	 * @param the new element
	 * @return the old vertex element
	 * @throws InvalidPositionException : if given vertex isn't in graph
	 * @see
	 * DESCRIPTION: Replaces the given vertex's element with the given element.
	 * ANALYSIS: The time complexity is O(v) because every vertex may be visited
	 *           to determine if the given vertex is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added vertExist method to check if vertex in graph
	 */
	@TimeComplexity("O(v)")
	@Override
	public V replace(Vertex<V> p, V o) throws InvalidPositionException {
		MyVertex<V,E> pEx = vertExist(p);

		if(pEx == null)
			throw new InvalidPositionException("The vertex does not exist in the graph");

		V ans = pEx.element();
		pEx.setElement(o);

		return ans;
	} //end of method: replace

	/**
	 * METHODNAME: replace
	 * @author Kathryn Weinand
	 * @param the edge whose element will be changed
	 * @param the new element
	 * @return the old edge element
	 * @throws InvalidPositionException : if given edge isn't in graph
	 * @see
	 * DESCRIPTION: Replaces the given edge's element with the given element.
	 * ANALYSIS: The time complexity is O(e) because every edge may be visited
	 *           to determine if the given edge is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(e)")
	@Override
	public E replace(Edge<E> p, E o) throws InvalidPositionException {
		MyEdge<V,E> pEx = edExist(p);

		if(pEx == null)
			throw new InvalidPositionException("The edge does not exist in the graph");

		E ans = pEx.element();
		pEx.setElement(o);

		return ans;
	} //end of method: replace

	/**
	 * METHODNAME: incidentEdges
	 * @author Kathryn Weinand
	 * @param a vertex of which to determine its incident edges
	 * @return an Iterable collection of the incident edges of the given vertex. If no matches
	 *         are found the returned Iterable is empty.
	 * @throws InvalidPositionException : if given vertex isn't in graph
	 * @see
	 * DESCRIPTION: Return an iterable collection of all incident edges of the given vertex.
	 * ANALYSIS: The time complexity is O(e) because every edge could be visited.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added vertExist method to check if vertex in graph
	 */
	@TimeComplexity("O(e)")
	@Override
	public Iterable<Edge<E>> incidentEdges(Vertex<V> v)
			throws InvalidPositionException {
		//check if given vertex is in a valid position
		MyVertex<V,E> vEx = vertExist(v);
		if(vEx == null)
			throw new InvalidPositionException("The vertex does not exist in the graph");

		return vEx.incidentEdges();

	} //end of method: incidentEdges

	/**
	 * METHODNAME: incidentEdgesPrint
	 * @author Kathryn Weinand
	 * @param a vertex of which to determine its incident edges
	 * @return
	 * @throws InvalidPositionException : if given vertex isn't in graph
	 * @see
	 * DESCRIPTION: Prints an iterable collection of all incident edges of the given vertex.
	 * ANALYSIS: The time complexity is O(e) because every edge could be visited.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added vertExist method to check if vertex in graph
	 */
	@TimeComplexity("O(e)")
	public void incidentEdgesPrint(Vertex<V> v){
		MyVertex<V,E> vEx = vertExist(v);
		if(vEx == null)
			throw new InvalidPositionException("The vertex does not exist in the graph");

		Iterator<Edge<E>> list = vEx.incidentEdges().iterator();
		System.out.print(v.toString() + " Incidient Edges: [");
		while(list.hasNext()){
			System.out.print(list.next().toString() + ", ");
		}
		System.out.println("]");
	}

	/**
	 * METHODNAME: endVertices
	 * @author Kathryn Weinand
	 * @param a edge of which to find its end vertices
	 * @return an array of the end vertices of the given edge
	 * @throws InvalidPositionException : if given edge isn't in graph
	 * @see
	 * DESCRIPTION: Return an array of the given edge's end vertices.
	 * ANALYSIS: The time complexity is O(e) because every edge may be visited
	 *           to determine if the given edge is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(e)")
	@Override
	public Vertex[] endVertices(Edge<E> e) throws InvalidPositionException {
		//Does edge exist?
		MyEdge<V,E> eEx = edExist(e);

		if(eEx == null)
			throw new InvalidPositionException("The edge does not exist in the graph.");

		Vertex[] list = ((MyEdge<V,E>)e).endVertices();
		assert list.length == 2 : "There should only be two end vertices";
		return list;
	} //end of method: endVertices

	/**
	 * METHODNAME: opposite
	 * @author Kathryn Weinand
	 * @param one vertex of the given edge
	 * @param an edge
	 * @return the edge's other vertex (not the given one)
	 * @throws InvalidPositionException : if given edge isn't in graph
	 * @see
	 * DESCRIPTION: Returns the given edge's opposite vertex of the given vertex.
	 * ANALYSIS: The time complexity is O(e) because every edge may be visited
	 *           to determine if the given edge is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(e)")
	@Override
	public Vertex<V> opposite(Vertex<V> v, Edge<E> e)
			throws InvalidPositionException {
		//Does edge exist?
		MyEdge<V,E> eEx = edExist(e);

		if(eEx == null)
			throw new InvalidPositionException("The edge does not exist in the graph.");

		Vertex<V>[] list = ((MyEdge<V,E>)e).endVertices();
		assert list.length == 2 : "There should only be two end vertices";
		if(list[0].equals(v)){
			return list[1];
		}
		else if(list[1].equals(v)){
			return list[0];
		}
		else{
			throw new InvalidPositionException("The given vertex is not associated with this edge");
		}
	} //end of method: opposite

	/**
	 * METHODNAME: areAdjacent
	 * @author Kathryn Weinand
	 * @param a vertex
	 * @param another vertex
	 * @return true if the vertices are adjacent; false otherwise
	 * @throws InvalidPositionException : if given vertex isn't in graph
	 * @see
	 * DESCRIPTION: Determines whether the given vertices are adjacent.
	 * ANALYSIS: The time complexity is O(v) because every vertex may be visited
	 *           to determine if the given vertices are in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added vertExist method to check if vertex in graph\
	 */
	@TimeComplexity("O(v)")
	@Override
	public boolean areAdjacent(Vertex<V> u, Vertex<V> v)
			throws InvalidPositionException {
		MyVertex<V,E> one = vertExist(u);
		MyVertex<V,E> two = vertExist(v);

		if(one == null || two == null)
			throw new InvalidPositionException("One or both of the vertices don't exist in the graph.");

		Iterator<Edge<E>> list = one.incidentEdges().iterator();
		while(list.hasNext()){
			Edge<E> curr = list.next();
			Vertex<V> match = opp(one,curr);
			if(match.equals(two)){
				return true;
			}
		}
		return false;
	} //end of method: areAdjacent

	/**
	 * METHODNAME: opp
	 * @author Kathryn Weinand
	 * @param one vertex of the given edge
	 * @param an edge
	 * @return the edge's other vertex (not the given one)
	 * @throws InvalidPositionException : if given edge isn't in graph
	 * @see
	 * DESCRIPTION: Returns the given edge's opposite vertex of the given vertex.
	 *              This is the same method as opposite, but without the "edge
	 *              exists in graph" check.
	 * ANALYSIS: The time complexity is O(1) - size doesn't matter.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	public Vertex<V> opp(Vertex<V> v, Edge<E> e){
		Vertex<V>[] list = ((MyEdge<V,E>)e).endVertices();
		assert list.length == 2 : "There should only be two end vertices";
		if(list[0].equals(v)){
			return list[1];
		}
		else{
			return list[0];
		}
	} //end of method: opp

	/**
	 * METHODNAME: insertVertex
	 * @author Kathryn Weinand
	 * @param an element for a vertex
	 * @return the added vertex
	 * @throws
	 * @see
	 * DESCRIPTION: Adds a vertex to the graph with the given element.
	 * ANALYSIS: The time complexity is O(1) because it just adds to a list.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(1)")
	@Override
	public Vertex<V> insertVertex(V o) {
		MyVertex<V,E> ins = new MyVertex<V,E>(o);
		vert.addLast(ins);
		assert vert.last().element().equals(ins) : "Vertex not inserted";
		return ins;
	} //end of method: insertVertex

	/**
	 * METHODNAME: insertEdge
	 * @author Kathryn Weinand
	 * @param a vertex
	 * @param another vertex
	 * @param an element for a edge
	 * @return the added edge
	 * @throws InvalidPositionException - if given vertices aren't in graph
	 * @see
	 * DESCRIPTION: Adds a edge to the graph between the given vertices with
	 *              the given element.
	 * ANALYSIS: The time complexity is O(v) because every vertex may be visited
	 *           to determine if the given vertices are in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added vertExist method to check if vertex in graph
	 */
	@TimeComplexity("O(v)")
	@Override
	public Edge<E> insertEdge(Vertex<V> u, Vertex<V> v, E o)
			throws InvalidPositionException {
		//Do vertices exist in graph?
		MyVertex<V,E> uEx = vertExist(u);
		MyVertex<V,E> vEx = vertExist(v);

		if(uEx == null || vEx == null)
			throw new InvalidPositionException("One or both of the vertices don't exist in the graph.");

		MyEdge<V,E> ins = new MyEdge<V,E>(u,v,o);
		ed.addLast(ins);
		assert ed.last().element().equals(ins) : "Edge not inserted";
		if(u.toString().equals(v.toString())){
			((MyVertex<V,E>)u).insertIncidence(ins);
		}
		else{
			((MyVertex<V,E>)u).insertIncidence(ins);
			((MyVertex<V,E>)v).insertIncidence(ins);
		}
		return ins;
	} //end of method: insertEdge

	/**
	 * METHODNAME: removeVertex
	 * @author Kathryn Weinand
	 * @param a vertex
	 * @return the removed vertex's element
	 * @throws InvalidPositionException - if given vertex isn't in graph
	 * @see
	 * DESCRIPTION: Removes the given vertex from the graph. All edges connecting
	 *              to this vertex will also be deleted.
	 * ANALYSIS: The time complexity is O(v) because every vertex may be visited
	 *           to determine if the given vertex is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added isEmpty checks!
	 */
	@TimeComplexity("O(v)")
	@Override
	public V removeVertex(Vertex<V> v) throws InvalidPositionException {
		Position<MyVertex<V,E>> vic = null;
		//find correct position in vert list for removal later
		if(!vert.isEmpty()){
			Position<MyVertex<V,E>> curr = vert.first();
			while(curr != vert.last()){
				if(curr.element().toString().equals(v.toString())){
					vic = curr;
					break;
				}
				curr = vert.next(curr);
			}
			if(vert.last().element().toString().equals(v.toString())){
				vic = vert.last();
			}
		}

		if(vic == null){
			throw new InvalidPositionException("The vertex does not exist in the graph");
		}

		MyVertex<V,E> victim = vic.element();
		V ans = victim.element();

		//delete all edges associated with this vertex
		Iterator<Edge<E>> edg = victim.incidentEdges().iterator();
		while(edg.hasNext()){
			Edge<E> curr = edg.next();
			removeEdge(curr);
		}
		//remove the vertex
		vert.remove(vic);
		assert vertExist(v) == null : "Vertex not removed";
		return ans;
	} //end of method: removeVertex

	/**
	 * METHODNAME: removeEdge
	 * @author Kathryn Weinand
	 * @param an edge
	 * @return the removed edge's element
	 * @throws InvalidPositionException - if given edge isn't in graph
	 * @see
	 * DESCRIPTION: Removes the given edge from the graph.
	 * ANALYSIS: The time complexity is O(v+e) because every edge and vertex may
	 *           be visited to determine if the given edge and its end vertices
	 *           are in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/11/25 Kathryn Weinand: Added vertExist/edExist methods to check
	 *                               if vertex/edge are in graph
	 */
	@TimeComplexity("O(e)")
	@Override
	public E removeEdge(Edge<E> e) throws InvalidPositionException {
		MyEdge<V,E> eEx = edExist(e);

		if(eEx == null)
			throw new InvalidPositionException("The edge does not exist in the graph.");

		E ans = e.element();

		//need the end vertices to remove their incidence to the removed edge
		MyVertex<V,E>[] list = ((MyEdge<V,E>)e).endVertices();
		MyVertex<V,E> l = vertExist(list[0]);
		MyVertex<V,E> r = vertExist(list[1]);

		//remove incidence from first end vertex
		boolean brek = false;
		NodePositionList<Edge<E>> ll = (NodePositionList<Edge<E>>) l.incidentEdges();
		Position<Edge<E>> curr = ll.first();
		while(curr != ll.last()){
			if(curr.element().toString().equals(e.toString())){
				l.removeIncidence(curr);
				brek = true;
				break;
			}
			curr = ll.next(curr);
		}
		if(!brek && curr.element().toString().equals(e.toString())){
			l.removeIncidence(curr);
		}
		//remove incidence from second end vertex
		brek = false;
		NodePositionList<Edge<E>> rl = (NodePositionList<Edge<E>>) r.incidentEdges();
		curr = rl.first();
		while(curr != rl.last()){
			if(curr.element().toString().equals(e.toString())){
				r.removeIncidence(curr);
				brek = true;
				break;
			}
			curr = rl.next(curr);
		}
		if(!brek && curr.element().toString().equals(e.toString())){
			r.removeIncidence(curr);
		}
		//remove edge
		brek = false;
		Position<MyEdge<V,E>> me = ed.first();
		while(me != ed.last()){
			if(me.element().toString().equals(e.toString())){
				ed.remove(me);
				brek = true;
				break;
			}
			me = ed.next(me);
		}
		if(!brek && me.element().toString().equals(e.toString())){
			ed.remove(me);
		}

		assert edExist(e) == null : "Edge not removed";
		return ans;
	} //end of method: removeEdge

	//TODO COMMENT
	public E removeEdge(E e){
		return removeEdge(edExist(e));
	}
	/**
	 * METHODNAME: toString
	 * @author Kathryn Weinand
	 * @param
	 * @return a string representation of both the vertex and edge lists.
	 * @throws
	 * @see
	 * DESCRIPTION: Returns a string representation of both the vertex and edge lists.
	 * ANALYSIS: The time complexity is O(v+e) because every vertex and every edge will
	 *           be visited
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/12/03 Kathryn Weinand: Used toStrings of vertices and edges
	 */
	@TimeComplexity("O(v+e)")
	@Override
	public String toString(){
		return "Vertices: " + vertices().toString() + "\nEdges: " + edges().toString();
	} //end of method: toString

	/**
	 * METHODNAME: vertExist
	 * @author Kathryn Weinand
	 * @param a vertex
	 * @return the same vertex if it exists in the graph; null otherwise.
	 * @throws
	 * @see
	 * DESCRIPTION: Determine if vertex exists in graph. Returns the same vertex
	 *              if it exists in the graph; null otherwise.
	 * ANALYSIS: The time complexity is O(v) because every vertex may be visited
	 *           to determine if the given vertex is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(v)")
	public MyVertex<V, E> vertExist(Vertex<V> v){
		Iterator<MyVertex<V,E>> list = vert.iterator();
		while(list.hasNext()){
			MyVertex<V,E> curr = list.next();
			if(curr.equals(v)){
				return curr;
			}
		}
		return null;
	} //end of method: vertExist

	//TODO COMMENT
	public MyVertex<V, E> vertExist(V v){
		Iterator<MyVertex<V,E>> list = vert.iterator();
		while(list.hasNext()){
			MyVertex<V,E> curr = list.next();
			//Some elements are null, and we find those by edges. We never find them this way.
			if(curr.element() != null && curr.element().equals(v)){
				return curr;
			}
		}
		return null;
	} //end of method: vertExist

	/**
	 * METHODNAME: edExist
	 * @author Kathryn Weinand
	 * @param an edge
	 * @return the same edge if it exists in the graph; null otherwise.
	 * @throws
	 * @see
	 * DESCRIPTION: Determine if edge exists in graph. Returns the same edge
	 *              if it exists in the graph; null otherwise.
	 * ANALYSIS: The time complexity is O(e) because every edge may be visited
	 *           to determine if the given edge is in the graph.
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(e)")
	public MyEdge<V, E> edExist(Edge<E> e){
		Iterator<MyEdge<V,E>> l = ed.iterator();
		while(l.hasNext()){
			MyEdge<V,E> curr = l.next();
			if(curr.equals(e)){
				return curr;
			}
		}
		return null;
	} //end of method: edExist
	
	/**
	 * edExist
	 * @param e edge value
	 * @return edge if it exists
	 */
	@TimeComplexity("O(e)")
	public MyEdge<V, E> edExist(E e){
		Iterator<MyEdge<V,E>> l = ed.iterator();
		while(l.hasNext()){
			MyEdge<V,E> curr = l.next();
			if(curr.element().equals(e)){
				return curr;
			}
		}
		return null;
	} //end of method: edExist

	/**
	 * METHODNAME: print
	 * @author Kathryn Weinand
	 * @param
	 * @return
	 * @throws
	 * @see
	 * DESCRIPTION: Prints a string representation of both the vertex and edge lists.
	 * ANALYSIS: The time complexity is O(v+e) because every vertex and every edge will
	 *           be visited
	 * MODIFICATIONS:
	 *   2013/11/25 Kathryn Weinand: Created
	 *   2013/12/03 Kathryn Weinand: Used toString
	 */
	@TimeComplexity("O(v+e)")
	public void print(){
		System.out.println(toString());
	} //end of method: print

	/**
	 * METHODNAME: shortestPath
	 * @author Kathryn Weinand
	 * @param current position vertex
	 * @param target vertex
	 * @return the next edge from the current position vertex on the shortest path to the target
	 * @throws InvalidPositionException : when the current or target vertices aren't in graph
	 * @see
	 * DESCRIPTION: Finds the shortest path from the given current position to the given
	 *              target position. For the purposes of Programming Assignment 8, the
	 *              next edge of the path from the start is all the is needed, and thus
	 *              is the return value.
	 * ANALYSIS: The time complexity is O(v+e) because every vertex and every edge can
	 *           be visited
	 * MODIFICATIONS:
	 *   2013/12/05 Kathryn Weinand: Created
	 */
	@TimeComplexity("O(v+e)")
	public E shortestPath(MyVertex<V,E> curr, MyVertex<V,E> target) throws InvalidPositionException {
		//vertExist is called on the vertices before this method is called.
		if(curr == null || target == null)
			throw new InvalidPositionException("One or both the vertices are not in the graph");

		//Reset each vertex
		Iterator<Vertex<V>> ver = vertices().iterator();
		while(ver.hasNext()){
			MyVertex<V,E> v = (MyVertex<V, E>) ver.next();
			v.setDist(1000000);
			v.setPrev(null);
			v.setPEdge(null);
			v.setVisit(false);
		}
		//Conduct Breadth First-like Search
		MyQueue<MyVertex<V,E>> que = new MyQueue<MyVertex<V,E>>(); //because graph is unweighted, this is a regular queue, not a priority queue
		curr.setDist(0);
		que.enqueue(curr);
		while(!que.isEmpty()){
			MyVertex<V,E> u = que.dequeue(); //I believe I can do it this way because the graph is unweighted
			u.setVisit(true);
			Iterator<Edge<E>> list = incidentEdges(u).iterator();
			while(list.hasNext()){
				Edge<E> e = list.next();
				MyVertex<V,E> v = (MyVertex<V, E>) opposite(u, e);
				int alt = u.getdist()+1; //Graph is unweighted
				if (alt < v.getdist()){
					v.setDist(alt);
					v.setPrev(u);
					v.setPEdge((MyEdge<V, E>) e);
					if(!v.getVisit()){
						que.enqueue(v);
					}
				}
			}
		}

		//Construct shortest path with edges
		NodePositionList<MyEdge<V,E>> s = new NodePositionList<MyEdge<V,E>>();
		//NodePositionList<MyVertex<V,E>> seq = new NodePositionList<MyVertex<V,E>>();
		MyVertex<V,E> c = target;
		while(c != null){
			s.addFirst(c.getPEdge());
			//seq.addFirst(c);
			c = c.getPrev();
		}

		return s.next(s.first()).element().element();
	} //end of method: shortestPath

	

	//TODO COMMENT
	public String toDotFormat(){
		int nullCount = 0;
		String ret = "graph G {\n";
		for(Edge<E> edgeEdge : edges()){
			MyEdge<V,E> edge = (MyEdge)edgeEdge;//Weee, casting.
			String vert0 = "";
			if(edge.endVertices()[0].element() == null){
				vert0 = "null" + nullCount;
				nullCount++;
			}else{
				vert0 = edge.endVertices()[0].toString();
			}
			String vert1 = "";
			if(edge.endVertices()[1].element() == "null"){
				vert1 = "null" + nullCount;
				nullCount++;
			}else{
				vert1 = edge.endVertices()[1].toString();
			}
			String colour = "";
			if(edge.endVertices()[1].isRecommended() || edge.endVertices()[0].isRecommended()){
				colour = "color=red";
			}
			ret += "\t" + vert0  + " -- " + vert1 + " [label=\"" + edge.element() + "\"" + colour + "];\n";
		}
		return ret + "}";
	}
	
	public void clearRecommended(){
		for(Vertex<V> v : vert){
			((MyVertex<V,E>)v).resetRecommended();
		}
	}
} //end of class: MainGraph
